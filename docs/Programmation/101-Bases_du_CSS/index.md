# Bases du CSS

Traduit et adapté de "CSS cheat sheet" par l'équipe DuckDuckHack

## Insertion d'une feuille de style

```html title="Feuille de style externe"
<link rel="stylesheet" type="text/css" href="style.css" />
```

```html title="Style en inline"
<tag style="property: value">
```

## Syntaxe générale

```css title="Syntaxe basique d'un bloc de déclaration CSS"
selecteur {
    propery: value;
}
```

```css title="Sélecteur de classe"
.class{}
```

Cela sélectionne tous les éléments avec l'atribut class="class" et leur applique le style

```css title="Sélecteur d'id"
#id{}
```

Cela sélectionne tous les éléments avec l'atribut id="id" et leur applique le style

## Propriétés de texte

`letter-spacing` : Augemente ou diminue l'espace entre des caractères

`line-height` : Spécifie la hauteur de la ligne

`text-align` : Spécifie l'alignement horisontal du texte d'un élément

`text-decoration` : Spécifie la décoration a appliquer au texte

`text-transform` : Controle la capitalisation dans un texte

`vertical-align` : Définit l'alignement vertical du texte

`word-spacing` : Augmente ou diminue l'espace entre les mots

`direction` : Change la dirrection du texte d'un élément.

## Propriétés de police

`font-family` : Les polices a utiliser

`font-size` : La taille de la police

`font-weight` : Définit l'épaisseur des caractères d'un texte

`color` : Change la couleur du texte

## Propriété d'arrière plan

`background-color` : définit la couleur de fond d'un élément

`background-image` : Spécifie une image a utiliser comme fond de l'élément

`background-repeat` : Spécifie comment une immage doit de répéter (axe x ou y)

`background-attachement` : Définit si une image est fixe ou se déplace avec le défilement de la page

`background-position` : Définit la position de démarrage de l'image de font


Plus d'infos sur [w3school](https://www.w3schools.com/css/default.asp)