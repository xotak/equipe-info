# Commandes de bases

Adapté du cours de [NSI](https://saint-francois-xavier.fr/nsi). Cette page va présenter les commandes les plus utiles et à connaitre pour pouvoir utiliser la ligne de commande sur Linux de façon idéale. Il est a noter que le `$` et `#` ne sont pas a recopier. Ces caractères indiquent juste le niveau de permission nécessaire pour utiliser la commande, $ étant pour l'utilisateur normal et `#` pour l'utilisateur `root`. Cette liste n'est évidamment pas exhaustive. Pour plus de commandes dont quelques unes plus avancées.

## Commandes de base et navigation dans l'arboressence

### echo

Cette commande permet d'écrire quelque chose dans le terminal

```bash
$ echo "test"
test
```

### ls

Liste les fichiers d'un dossiers. Options : `-a` pour les fichiers cachés, `-l` pour la liste détaillées, `-h` pour les tailles en unités "human readable". Très pratique l'option -R permet en un coup d'œil de visualiser les sous-dossiers.

```bash
$ ls
fichier1 fichier2 fichier3
```

```bash
$ ls -la
total 766548
drwxrwxr-x  2 user user      4096 oct.  19 10:40 .
drwxr-x--- 66 user user      4096 oct.  19 10:38 ..
-rw-rw-r--  1 user user 131858432 oct.  19 10:39 fichier1
-rw-rw-r--  1 user user 150208512 oct.  19 10:40 fichier2
-rw-rw-r--  1 user user 502857728 oct.  19 10:40 fichier3

```

### pwd

*print working directory*. Cette commande affiche tout simplement le chemin absolu du dossier dans lequel on se trouve

```bash
$ pwd
/
```

## Opérations sur les fichiers

### cat

Lire le contenu d'un fichier

```bash
$ cat fichier.txt
Je suis un fichier
```

### touch

Permet de créer un fichier

```bash
$ touch fichier.txt
(Pas de sortie)
```

### mkdir

Même fonctionnement de `touch`, mais créée un dossier

```bash
$ mkdir fichier.txt
(Pas de sortie)
```

### cp

`copy`, faire une copie d'un fichier. L'option -R permet de réaliser des copies de dossiers entiers.

```bash
$ cp fichier.txt fichier_copie.txt
(Pas de sortie)
```

On peut aussi placer la copie directement dans un autre dossier

```bash
$ cp fichier.txt dossier/fichier_copie.txt
(Pas de sortie)
```
### mv

`move`, permet de déplacer des fichiers/dossiers. La commande `mv` s'utilise exactement de la même manière que la commande `cp`. En outre, cette commande permet aussi de renommer les fichiers et dossiers en changeant simplement leur nom.

```bash
$ mv fichier.txt fichier_renommé.txt
(Pas de sortie)
```
## Flux de redirrection

### >

Permet de renvoyer le résultat d'une commande dans un fichier

```bash
$ echo "Bonjour" > fichier.txt
(Pas de sortie)
$ cat fichier.txt
Bonjour
```

## Droits, groupes et utilisateurs

### sudo

Permet d'exécuter une commande en tant qu'administrateur

### sudo su

Permet de se connecter au compte root grace au mot de passe de l'utilisateur courrant

```bash
$ sudo su
(Pas de sortie)
#
```

### su

*switch user*, permet de se connecter au compte passé en argument

Executer su root permet de se connecter au compte root

```bash
$ su root
(Pas de sortie)
#
```